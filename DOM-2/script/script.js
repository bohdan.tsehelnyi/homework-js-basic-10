// =================THEORY====================
/*
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
Способи для сворення елементу:
document.createElement();
document.createTextNode();

пособи для додавання нових елементів
innerHTML
innerText
textContent
insertAdjcentHTML, Element, Text (beforebegin, afterbegin, beforeend, afterend);
append, prepend, after, before

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

const nav = document.querySelector('.navigation');
nav.remove();

якщо є дочірнім елементом:
parentElement.removeChild(nav);
parentElement.textContent = '';

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
element.before(node,strings)
element.after(node,strings)
element.append(node,strings)
element.prepend(node,strings)

або

insertAdjacentHTML, Element, Text (beforebegin, afterbegin, beforeend, afterend);
*/

// =================PRACTICE======================

// 1.

const footer = document.querySelector('footer');

const link = document.createElement('a');
link.textContent = "Learn More";
link.style.cssText = 'color: #fff; text-decoration: none;'; 
link.setAttribute("href", "#");
footer.append(link);

// 2.

const select = document.createElement('select');

select.setAttribute('id', 'raiting');
const main = document.querySelector('main');

main.prepend(select);

// 3.


const fragment = document.createDocumentFragment();

for(let i = 1; i <= 4; i++){
	let option = document.createElement('option');
	option.setAttribute('value', i);
	option.textContent = `${i} Star`;
	fragment.append(option);
}

select.append(fragment);


// select.innerHTML = '<option>4 Stars</option><option>3 Stars</option><option>2 Stars</option><option>1 Stars</option>';
// option.setAttribute('value', '1');


